function printHello() {
  console.log('Hello');
}

function runTimeout() {

  // Use setTimeout to print "Hello" to the console after 250ms.
  return setTimeout(printHello, 250);

}

export default runTimeout;
