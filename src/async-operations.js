function asyncFunction() {
  // return new Promise((resolve) => {
  //   resolve('asyncFunction Done!');
  // });

  return Promise.resolve('asyncFunction Done!');
}

function doAsyncFunction() {
  asyncFunction().then((result) => {
    console.log(result);
  });

  console.log('Called after asyncFunction, but happens before asyncFunction completes');
}

export default doAsyncFunction;
