function getStep(step) {
  return new Promise(resolve => {
    resolve(`Step ${step}`);
  });
}

const step1 = getStep(1);
const step2 = getStep(2);
const step3 = getStep(3);

function runSteps() {
  const steps = [step1, step2, step3];

  // Do them all!
  Promise.all(steps.map(step => {
    step.then(console.log);
  }));

  // Promise.all(steps).then(results => {
  //   results.forEach(result => {
  //     console.log(result);
  //   });
  // });
}

export default runSteps;
