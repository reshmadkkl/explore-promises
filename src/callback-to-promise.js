import fs from 'fs';
import path from 'path';
import util from 'util';

function readFile() {
  return new Promise((resolve, reject) => {
    const filePath = path.join(__dirname, 'data', 'file.txt');
    fs.readFile(filePath, 'utf8', (err, data) => {
      if(err) {
        reject(err);
      }
      resolve(data);
    });
  });
}

function printContents() {
  readFile().then(console.log);
}

export default printContents;
