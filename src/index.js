import doAsyncFunction from './async-operations';
import runTimeout from './callbacks';
import resolvePromise from './promise-resolved';
import rejectPromise from './promise-rejected';
import promiseThenPromise from './promise-then-promise';
import printContents from './callback-to-promise';
// import runAsync from './async-await';
import promiseAll from './promise-all';
import promiseRace from './promise-race';

// doAsyncFunction();
// runTimeout();
// resolvePromise();
// rejectPromise();
// promiseThenPromise();
// printContents();
// runAsync();
// promiseAll();
promiseRace();
