
const step1 = Promise.resolve('Step 1'); // Comment out this line and uncomment the next to demo Promise rejection
// const step1 = Promise.reject(new Error('Step 1 Fail'));

const step2 = Promise.resolve('Step 2');
const step3 = Promise.resolve('Step 3');

// Run each step printing results along the way
function runSteps() {
  // Your code

  step1
    .then(console.log).catch(err => { console.log(err.message); })
    .then(() => { return step2.then(console.log); })
    .then(() => { return step3.then(console.log); });

  // Or, using the second argument to .then as the rejection handler
  // step1
  //   .then(console.log, (err) => { console.log(err.message); })
  //   .then(() => { return step2.then(console.log); })
  //   .then(() => { return step3.then(console.log); });

}

export default runSteps;
